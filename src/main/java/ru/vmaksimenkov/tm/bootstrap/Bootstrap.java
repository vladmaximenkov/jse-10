package ru.vmaksimenkov.tm.bootstrap;

import ru.vmaksimenkov.tm.api.*;
import ru.vmaksimenkov.tm.constant.ArgumentConst;
import ru.vmaksimenkov.tm.constant.TerminalConst;
import ru.vmaksimenkov.tm.controller.CommandController;
import ru.vmaksimenkov.tm.controller.ProjectController;
import ru.vmaksimenkov.tm.controller.TaskController;
import ru.vmaksimenkov.tm.repository.CommandRepository;
import ru.vmaksimenkov.tm.repository.ProjectRepository;
import ru.vmaksimenkov.tm.repository.TaskRepository;
import ru.vmaksimenkov.tm.service.CommandService;
import ru.vmaksimenkov.tm.service.ProjectService;
import ru.vmaksimenkov.tm.service.TaskService;
import ru.vmaksimenkov.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);


    public void run(final String... args) {
        commandController.showWelcome();
        if (parseArgs(args)) System.exit(0);

        while (true) {
            System.out.println("Enter command: ");
            final String command = TerminalUtil.SCANNER.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP: commandController.showHelp(); break;
            case ArgumentConst.ARG_VERSION: commandController.showVersion(); break;
            case ArgumentConst.ARG_ABOUT: commandController.showAbout(); break;
            case ArgumentConst.ARG_INFO: commandController.showSystemInfo(); break;
            default: incorrectCommand();
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP: commandController.showHelp(); break;
            case TerminalConst.CMD_VERSION: commandController.showVersion(); break;
            case TerminalConst.CMD_ABOUT: commandController.showAbout(); break;
            case TerminalConst.CMD_EXIT: commandController.sysExit(); break;
            case TerminalConst.CMD_INFO: commandController.showSystemInfo(); break;
            case TerminalConst.CMD_COMMANDS: commandController.showCommands(); break;
            case TerminalConst.CMD_ARGUMENTS: commandController.showArguments(); break;
            case TerminalConst.CMD_TASK_LIST: taskController.showList(); break;
            case TerminalConst.CMD_TASK_CREATE: taskController.create(); break;
            case TerminalConst.CMD_TASK_CLEAR: taskController.clear(); break;
            case TerminalConst.CMD_PROJECT_LIST: projectController.showList(); break;
            case TerminalConst.CMD_PROJECT_CREATE: projectController.create(); break;
            case TerminalConst.CMD_PROJECT_CLEAR: projectController.clear(); break;
            default: incorrectCommand();
        }
    }

    public void incorrectCommand() {
        System.out.println("Unknown command. Type help to see all available commands");
    }

    public void incorrectArgument() {
        System.out.println("Unknown argument. Type -h to see all available arguments");
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
