package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements ru.vmaksimenkov.tm.api.IProjectRepository {

    private final List<Project> list = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return list;
    }

    @Override
    public void add(Project project) {
        list.add(project);
    }

    @Override
    public void remove(final Project project) {
        list.remove(project);
    }

    @Override
    public void clear() {
        list.clear();
    }

}
